console.log('Hello World')

//Objects 

//creating objects using object initializers/literal notation
//{} => Object Literals

let cellphone = {
	name: 'Nokia 3310',
	manufactureDate: 1999
}

console.log(cellphone);
console.log(typeof cellphone);

//access using dot notation
console.log(cellphone.name)

let cellphone2 = {
	name: 'RealMe Super Zoom'
	manufactureDate: 2019
}

let cellphone3 = {
	name: 'Samsung A7',
	manufactureDate: 2017
}



//Creating objects using a constructor function 
// Creates a reusable function to create several objects that have the same data structure a
/*
Syntax:
	function objectName(keyA, keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}
*/

function Laptop(name, os, price) {
	this.name = name;
	this.os = os;
	this.price = price;
}

//This is a unique instance of the laptop object
let laptop1 = new Laptop('Lenovo', 'Windows 10', 30000);
console.log(laptop1);