console.log("Hello World");

let trainer = {
	name: 'Judy Lyn Medalla'
	age: 23
	pokemon: ['pikachu','blastoise', 'kakuna']
	friends: ['hannah', 'marie', 'mandy']


}

trainer1 = {
	talk: function(){
		console.log(`Hello my name is ${this.name}`)
	}
}

trainer1.talk();
console.log(trainer1);


function Pokemon(name, level, health, attack) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		console.log("targetPokemon's health is now reduced.")
	};
	this.faint = function() {
		console.log(`${this.name} fainted.`)
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
rattata.tackle(pikachu);


rattata.faint();

